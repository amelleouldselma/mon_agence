Notre kanban pour suivre le projet : https://trello.com/b/OSMCJugz/notre-kanban

## Installation

- npm install parcel-bundler
- npm init
- npm install bulma

## Changement de framework

- npm start
- npm install materialize-css@next

## Surge

-git clone git@gitlab.com:amelleouldselma/mon_agence.git
-cd

*Version en ligne*

- https://mon_agence.surge.sh/


*Nouvelle version*

- http://mon-agence.surge.sh/


## Diagnostic performance 

- https://developers.google.com/speed/pagespeed/insights/?hl=fr&url=http%3A%2F%2Fmon_agence.surge.sh%2F&tab=desktop 

- 95%

## L'impact écologique

-https://www.website-footprint.com/en/result?w=https:%2F%2Fmon_agence.surge.sh%2F 

- 66%
